# Welcome to Docker

This is a repo for new users getting started with MiniKube and want to run a simple container.

You can try it out using the following steps.

1) Clone the repo.
```
git clone https://gitlab.com/hodonnell/welcome-to-minikube.git
```

2) Start Minikube
```
minikube start
```

2) Run the following command to build the container inside of Docker.
```
docker build -t welcome-to-minikube .
```

3) To use a Docker image in Minikube, you need to make sure Minikube's Docker daemon can access it. 
```
eval $(minikube -p minikube docker-env)    
```

4) Apply the deployment configuration to Minikube.
```
kubectl apply -f deployment.yaml
```

5) Expose  the service.
```
kubectl expose deployment my-deployment --type=NodePort --port=80
```

6) Finally, use Minikube's IP and NodePort to open the application in your browser.
```
minikube service my-deployment
```

And open `http://localhost:8088` in your browser.

# Building

Maintainers should see [MAINTAINERS.md](MAINTAINERS.md).

Build and run:
```
docker build -t welcome-to-docker . 
docker run -d -p 8088:3000 --name welcome-to-docker welcome-to-docker
```
Open `http://localhost:8088` in your browser.
